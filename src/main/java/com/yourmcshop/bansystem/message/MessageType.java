package com.yourmcshop.bansystem.message;

import com.yourmcshop.bansystem.BanSystem;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;

import java.lang.reflect.InvocationTargetException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by DevTastisch on 02.03.2019
 */
public enum MessageType {
    ERROR_PLAYER_ALREADY_BANNED("messageserror.alreadybanned", true),
    ERROR_PLAYER_ALREADY_MUTED("messageserror.alreadymuted", true),
    ERROR_PLAYER_NOT_BANNED("messageserror.notbanned", true),
    ERROR_PLAYER_NOT_MUTED("messageserror.notmuted", true),
    ERROR_NOT_FOUND_PLAYER("messageserror.playernotexists", true),
    ERROR_NO_NUMBER("messageserror.mustnumber", true),
    ERROR_NO_PERMISSIONS("messageserror.noperms", true),
    ERROR_INVALID_ID("messageserror.invalidid", true),
    ERROR_INVALID_PUNISH_ID("messageserror.invalidpunishid", true),
    SUCCESS_COMMAND_MUTE("messagessuccess.mute", true),
    SUCCESS_COMMAND_KICK("messagessuccess.kick", true),
    SUCCESS_COMMAND_REMOVEIP("messagessuccess.removeip", true),
    SUCCESS_COMMAND_UNPUNISH("messagessuccess.unpunish", true),
    SUCCESS_COMMAND_UNMUTE("messagessuccess.unmute", true),
    SUCCESS_COMMAND_BAN("messagessuccess.ban", true),
    SUCCESS_COMMAND_PBAN("messagessuccess.ban", true),
    SUCCESS_COMMAND_TBAN("messagessuccess.ban", true),
    SUCCESS_COMMAND_UNBAN("messagessuccess.unban", true),
    SUCCESS_COMMAND_CLEARPUNISHHISTORY("messagessuccess.clearpunishhistory", true),
    SUCCESS_COMMAND_SETPUNISH("messagessuccess.setpunish", true),
    COMMAND_SYNTAX_UNMUTE("correctsyntax.unmute", true),
    COMMAND_SYNTAX_PMUTE("correctsyntax.pmute", true),
    COMMAND_SYNTAX_TMUTE("correctsyntax.tmute", true),
    COMMAND_SYNTAX_BAN("correctsyntax.ban", true),
    COMMAND_SYNTAX_UNBAN("correctsyntax.unban", true),
    COMMAND_SYNTAX_TBAN("correctsyntax.tban", true),
    COMMAND_SYNTAX_PBAN("correctsyntax.pban", true),
    COMMAND_SYNTAX_CLEARPUNISHHISTORY("correctsyntax.clearpunishhistory", true),
    COMMAND_SYNTAX_CHECKPLAYER("correctsyntax.checkplayer", true),
    COMMAND_SYNTAX_KICK("correctsyntax.kick", true),
    COMMAND_SYNTAX_PUNISHLOG("correctsyntax.punishlog", true),
    COMMAND_SYNTAX_REMOVEIP("correctsyntax.removeip", true),
    COMMAND_SYNTAX_SETPUNISH("correctsyntax.setpunish", true),
    COMMAND_SYNTAX_UNPUNISH("correctsyntax.unpunish", true),
    INFO_MUTE_SCREEN("display.mutescreen", true),
    INFO_MUTE_SCREEN_PERMANENT("display.mutescreenpermanent", true),
    INFO_BAN_SCREEN("display.banscreen"),
    INFO_BAN_SCREEN_PERMANENT("display.banscreenpermanent"),
    INFO_KICK_SCREEN("display.kickscreen"),
    INFO_COMMAND_CHECK_BAN_INFO("check.checkban"),
    INFO_COMMAND_CHECK_MUTE_INFO("check.checkmute"),
    INFO_COMMAND_CHECK_PUNISH_INFO("check.checkpunish"),
    INFO_BROADCAST_BAN("broadcasts.ban", true),
    INFO_BROADCAST_MUTE("broadcasts.mute", true),
    INFO_BROADCAST_UNBAN("broadcasts.unban", true),
    INFO_BROADCAST_UNMUTE("broadcasts.unmute", true),
    INFO_BROADCAST_KICK("broadcasts.kick", true),
    FORMAT_DATE_TIME_LAYOUT("display.datetimelayout"),
    FORMAT_TIME_LAYOUT("display.timelayout.format"),
    FORMAT_TIME_LAYOUT_DAYS("display.timelayout.days"),
    FORMAT_TIME_LAYOUT_HOURS("display.timelayout.hours"),
    FORMAT_TIME_LAYOUT_MINUTES("display.timelayout.minutes"),
    FORMAT_TIME_LAYOUT_SECONDS("display.timelayout.seconds"),
    FORMAT_CONSOLE_BAN("display.consoleban"),
    FORMAT_CHECK_PUNISHLOG("check.punishlog.format");

    private final String message;
    private final boolean prefix;

    MessageType(String message) {
        this(message, false);
    }

    MessageType(String message, boolean prefix) {
        this.message = message;
        this.prefix = prefix;
    }

    public final void sendTo(CommandSender commandSender, Object... args) {
        commandSender.sendMessage(this.format(args));
    }

    public final String format(Object... args) {
        String newMessage = BanSystem.getInstance().getMessageController().getMessage(message);
        String regex = "\\[\\{(\\d)}\\.(\\S+)\\(\\)]";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(newMessage);

        while (matcher.find()) {
            Object lastObject = args[Integer.parseInt(matcher.group(1))];
            for (String s : matcher.group(2).split("\\.")) {
                if (!s.isEmpty()) {
                    try {
                        lastObject = lastObject.getClass().getDeclaredMethod(s.replace("()", "")).invoke(lastObject);
                    } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }
            }
            newMessage = newMessage.replace(matcher.group(0), lastObject.toString());
        }
        for (int i = 0; i < args.length; i++) {
            newMessage = newMessage.replaceAll("\\{"+ i +"}", args[i].toString());
        }
        return (BanSystem.getInstance().getMainConfig().getBoolean("settings.showprefix") && this.prefix ? ChatColor.translateAlternateColorCodes('&', BanSystem.getInstance().getMainConfig().getString("settings.prefix")) : "") +
                ChatColor.translateAlternateColorCodes('&', newMessage);
    }


}
