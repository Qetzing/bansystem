package com.yourmcshop.bansystem.command.punish;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishEntry;
import com.yourmcshop.bansystem.message.MessageType;
import com.yourmcshop.bansystem.model.DateTime;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.ExecutionException;

/**
 * Created by DevTastisch on 05.04.2019
 */
public class SetPunishCommand extends Command {

    public SetPunishCommand() {
        super("setpunish");
    }

    public void execute(CommandSender sender, String[] args) {
        if (!sender.hasPermission("bansystem.setpunish")) {
            MessageType.ERROR_NO_PERMISSIONS.sendTo(sender);
            return;
        }

        if (args.length == 3) {

            try {
                PunishEntry punishEntry = BanSystem.getInstance().getPunishEntryRepository().findById(args[0], false).get();
                if (punishEntry == null) {
                    MessageType.ERROR_INVALID_PUNISH_ID.sendTo(sender, args[0]);
                    return;
                }


                long duration = 0;
                long time = Integer.valueOf(args[1]);
                switch (args[2]) {
                    case "s":
                        duration += (time * 1000L);
                        break;
                    case "min":
                        duration += (time * 1000L * 60L);
                        break;
                    case "h":
                        duration += (time * 1000L * 60L * 60L);
                        break;
                    case "d":
                        duration += (time * 1000L * 60L * 60L * 24L);
                        break;
                    case "mo":
                        duration += (time * 1000L * 60L * 60L * 24L * 30L);
                        break;
                    case "y":
                        duration += (time * 1000L * 60L * 60L * 24L * 365L);
                        break;
                }

                if(duration != 0){
                    punishEntry.setEnding(new DateTime(punishEntry.getCreated().getMillis() + duration));
                    BanSystem.getInstance().getPunishEntryRepository().save(punishEntry);
                    MessageType.SUCCESS_COMMAND_SETPUNISH.sendTo(sender, punishEntry.getUser(), punishEntry);
                    return;
                }

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

        }
        MessageType.COMMAND_SYNTAX_SETPUNISH.sendTo(sender);

    }
}
