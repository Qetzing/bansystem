package com.yourmcshop.bansystem.command.punish;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishUser;
import com.yourmcshop.bansystem.message.MessageType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.ExecutionException;

public class CheckPlayerCommand extends Command {

    public CheckPlayerCommand() {
        super("checkplayer");
    }

    public void execute(CommandSender sender, String[] args) {
        if(!sender.hasPermission("bansystem.checkplayer")){
            MessageType.ERROR_NO_PERMISSIONS.sendTo(sender);
            return;
        }
        if (args.length == 1) {
            try {
                PunishUser punishUser = BanSystem.getInstance().getPunishUserRepository().findByCachedNameIgnoreCase(args[0]).get();
                if (punishUser == null) {
                    MessageType.ERROR_NOT_FOUND_PLAYER.sendTo(sender, args[0]);
                    return;
                }

                MessageType.INFO_COMMAND_CHECK_PUNISH_INFO.sendTo(sender,
                        punishUser.isBanned() ?
                                MessageType.INFO_COMMAND_CHECK_BAN_INFO.format(
                                        punishUser,
                                        punishUser.getActiveBan(),
                                        punishUser.getActiveBan().getInvoker() == null ? MessageType.FORMAT_CONSOLE_BAN.format() : punishUser.getActiveBan().getInvoker().getCachedName(),
                                        punishUser.getActiveBan().getReasonId() == -1 ? punishUser.getActiveBan().getCustomReason() : punishUser.getActiveBan().getReason().getName(),
                                        punishUser.getActiveBan().getReasonId() == -1 ? "-" : punishUser.getActiveBan().getReason().getId()
                                ) : MessageType.ERROR_PLAYER_NOT_BANNED.format(punishUser),
                        punishUser.isMuted() ?
                                MessageType.INFO_COMMAND_CHECK_MUTE_INFO.format(
                                        punishUser,
                                        punishUser.getActiveMute(),
                                        punishUser.getActiveMute().getInvoker() == null ? MessageType.FORMAT_CONSOLE_BAN.format() : punishUser.getActiveMute().getInvoker().getCachedName(),
                                        punishUser.getActiveMute().getReasonId() == -1 ? punishUser.getActiveMute().getCustomReason() : punishUser.getActiveMute().getReason().getName(),
                                        punishUser.getActiveMute().getReasonId() == -1 ? "-" : punishUser.getActiveMute().getReason().getId()
                                ) : MessageType.ERROR_PLAYER_NOT_MUTED.format(punishUser)

                );
                return;

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        MessageType.COMMAND_SYNTAX_CHECKPLAYER.sendTo(sender);
    }
}
