package com.yourmcshop.bansystem.command.mute;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishEntry;
import com.yourmcshop.bansystem.data.PunishUser;
import com.yourmcshop.bansystem.message.MessageType;
import com.yourmcshop.bansystem.model.PunishType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.ExecutionException;

/**
 * Created by DevTastisch on 17.03.2019
 */
public class UnmuteCommand extends Command {


    public UnmuteCommand() {
        super("unmute");
    }

    public void execute(CommandSender commandSender, String[] args) {
        if (!commandSender.hasPermission("bansystem.unmute")) {
            MessageType.ERROR_NO_PERMISSIONS.sendTo(commandSender);
            return;
        }
        if (args.length == 1) {
            try {
                PunishUser punishUser = BanSystem.getInstance().getPunishUserRepository().findByCachedNameIgnoreCase(args[0]).get();
                if (punishUser == null) {
                    MessageType.ERROR_NOT_FOUND_PLAYER.sendTo(commandSender, args[0]);
                    return;
                }

                PunishEntry punishEntry = punishUser.isMuted() ? punishUser.getActiveMute() : null;

                if (BanSystem.getInstance().getPunishController().unpunish(punishUser.getUuid(), PunishType.MUTE)) {
                    ProxyServer.getInstance().getPlayers().stream()
                            .filter(player -> player.hasPermission("bansystem.notify"))
                            .forEach(player -> MessageType.INFO_BROADCAST_UNMUTE.sendTo(player,
                                    punishUser.getCachedName(),
                                    commandSender.getName()
                            ));
                    MessageType.SUCCESS_COMMAND_UNMUTE.sendTo(commandSender, punishUser, punishEntry);
                } else {
                    MessageType.ERROR_PLAYER_NOT_MUTED.sendTo(commandSender, punishUser);
                }
                return;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        MessageType.COMMAND_SYNTAX_UNMUTE.sendTo(commandSender);
    }
}
