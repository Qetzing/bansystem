package com.yourmcshop.bansystem.command.mute;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishUser;
import com.yourmcshop.bansystem.message.MessageType;
import com.yourmcshop.bansystem.model.PunishType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

/**
 * Created by DevTastisch on 17.03.2019
 */
public class TMuteCommand extends Command {

    private final Pattern pattern = Pattern.compile("(?i)s|min|h|d|mo|y");

    public TMuteCommand() {
        super("tmute");
    }

    public void execute(CommandSender sender, String[] args) {
        if(!sender.hasPermission("bansystem.tmute")){
            MessageType.ERROR_NO_PERMISSIONS.sendTo(sender);
            return;
        }
        if (args.length == 4) {
            if (!pattern.matcher(args[2]).matches()) {
                MessageType.COMMAND_SYNTAX_TMUTE.sendTo(sender);
                return;
            }
            try {
                PunishUser punishUser = BanSystem.getInstance().getPunishUserRepository().findByCachedNameIgnoreCase(args[0]).get();
                if (punishUser == null) {
                    MessageType.ERROR_NOT_FOUND_PLAYER.sendTo(sender, args[0]);
                    return;
                }
                if (punishUser.isBanned()) {
                    MessageType.ERROR_PLAYER_ALREADY_MUTED.sendTo(sender, punishUser);
                    return;
                }

                long duration = 0;
                long time = Integer.valueOf(args[1]);
                switch (args[2]) {
                    case "s":
                        duration += (time * 1000L);
                        break;
                    case "min":
                        duration += (time * 1000L * 60L);
                        break;
                    case "h":
                        duration += (time * 1000L * 60L * 60L);
                        break;
                    case "d":
                        duration += (time * 1000L * 60L * 60L * 24L);
                        break;
                    case "mo":
                        duration += (time * 1000L * 60L * 60L * 24L * 30L);
                        break;
                    case "y":
                        duration += (time * 1000L * 60L * 60L * 24L * 365L);
                        break;
                }

                ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(punishUser.getUuid());
                if (BanSystem.getInstance().getPunishController().punish(sender, punishUser.getUuid(), args[3], PunishType.MUTE, duration, proxiedPlayer == null ? punishUser.getLastIp() : proxiedPlayer.getPendingConnection().getAddress().getHostName())){
                    punishUser = BanSystem.getInstance().getPunishUserRepository().findByUniqueID(punishUser.getUuid()).get();
                    MessageType.SUCCESS_COMMAND_MUTE.sendTo(sender, punishUser, punishUser.getActiveMute());
                }else{
                    MessageType.ERROR_PLAYER_ALREADY_MUTED.sendTo(sender, punishUser);
                }
                return;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        MessageType.COMMAND_SYNTAX_TMUTE.sendTo(sender);
    }
}
