package com.yourmcshop.bansystem.command.ban;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishUser;
import com.yourmcshop.bansystem.message.MessageType;
import com.yourmcshop.bansystem.model.PunishType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.ExecutionException;

public class PBanCommand extends Command {

    public PBanCommand() {
        super("pban");
    }

    public void execute(CommandSender commandSender, String[] args) {
        if (!commandSender.hasPermission("bansystem.pban")) {
            MessageType.ERROR_NO_PERMISSIONS.sendTo(commandSender);
            return;
        }
        if (args.length == 2) {
            try {
                PunishUser punishUser = BanSystem.getInstance().getPunishUserRepository().findByCachedNameIgnoreCase(args[0]).get();
                if (punishUser == null) {
                    MessageType.ERROR_NOT_FOUND_PLAYER.sendTo(commandSender, args[0]);
                    return;
                }

                ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(punishUser.getUuid());
                if (BanSystem.getInstance().getPunishController().punish(commandSender, punishUser.getUuid(), args[1], PunishType.BAN, -1, proxiedPlayer == null ? punishUser.getLastIp() : proxiedPlayer.getPendingConnection().getAddress().getHostName())) {
                    MessageType.SUCCESS_COMMAND_PBAN.sendTo(commandSender, punishUser, punishUser.getActiveBan());
                } else {
                    MessageType.ERROR_PLAYER_ALREADY_BANNED.sendTo(commandSender, punishUser);
                }
                return;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        MessageType.COMMAND_SYNTAX_PBAN.sendTo(commandSender);
    }

}
