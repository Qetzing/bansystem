package com.yourmcshop.bansystem.command.ban;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishUser;
import com.yourmcshop.bansystem.message.MessageType;
import com.yourmcshop.bansystem.model.PunishFormat;
import com.yourmcshop.bansystem.model.PunishTime;
import com.yourmcshop.bansystem.model.PunishType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.Comparator;
import java.util.concurrent.ExecutionException;

/**
 * Created by DevTastisch on 01.03.2019
 */
public class BanCommand extends Command {

    public BanCommand() {
        super("ban");
    }

    public void execute(CommandSender commandSender, String[] args) {

        if (!commandSender.hasPermission("bansystem.ban")) {
            MessageType.ERROR_NO_PERMISSIONS.sendTo(commandSender);
            return;
        }

        int id;
        if (args.length == 2) {
            try {
                id = Integer.valueOf(args[1]);
            } catch (Exception ex) {
                MessageType.ERROR_NO_NUMBER.sendTo(commandSender);
                return;
            }

            try {
                PunishUser punishUser = BanSystem.getInstance().getPunishUserRepository().findByCachedNameIgnoreCase(args[0]).get();
                if (punishUser == null) {
                    MessageType.ERROR_NOT_FOUND_PLAYER.sendTo(commandSender, args[0]);
                    return;
                }

                PunishFormat punishFormat = BanSystem.getInstance().getPunishController().getFormatById(id);
                if (punishFormat == null) {
                    MessageType.ERROR_INVALID_ID.sendTo(commandSender, id);
                    return;
                }

                if (!commandSender.hasPermission(punishFormat.getPermission())) {
                    MessageType.ERROR_NO_PERMISSIONS.sendTo(commandSender);
                    return;
                }
                ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(punishUser.getUuid());

                PunishTime nextTime = BanSystem.getInstance().getPunishController().getNextTime(punishUser.getUuid(), id);
                if (BanSystem.getInstance().getPunishController().punish(
                        commandSender,
                        punishUser.getUuid(),
                        id,
                        nextTime,
                        proxiedPlayer == null ? punishUser.getLastIp() : proxiedPlayer.getPendingConnection().getAddress().getHostName())) {
                    if (nextTime.getPunishType() == PunishType.BAN) {
                        MessageType.SUCCESS_COMMAND_BAN.sendTo(commandSender, punishUser, punishUser.getActivePunish(nextTime.getPunishType()));
                    } else {
                        MessageType.SUCCESS_COMMAND_MUTE.sendTo(commandSender, punishUser, punishUser.getActivePunish(nextTime.getPunishType()));
                    }
                } else {
                    MessageType.ERROR_PLAYER_ALREADY_BANNED.sendTo(commandSender, punishUser);
                }
                return;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        BanSystem.getInstance().getPunishController().getFormats().stream().sorted(Comparator.comparingInt(PunishFormat::getId)).forEachOrdered(punishFormat -> MessageType.COMMAND_SYNTAX_BAN.sendTo(commandSender,
                punishFormat
        ));

    }
}
