package com.yourmcshop.bansystem.controller;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.config.YamlConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DevTastisch on 02.03.2019
 */
public class MessageController {

    private final BanSystem banSystem;
    private final YamlConfig yamlConfig;

    public MessageController(BanSystem banSystem) {
        this.banSystem = banSystem;
        this.yamlConfig = YamlConfig.of(String.format("./plugins/%s/messages.yml", banSystem.getDescription().getName()));
    }

    public String getMessage(String key) {
        if (this.yamlConfig.getType(key).isAssignableFrom(ArrayList.class)) {
            List<String> stringList = this.yamlConfig.getStringList(key);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < stringList.size(); i++) {
                if (i == 0) {
                    stringBuilder
                            .append(stringList.get(i));
                } else {
                    stringBuilder
                            .append("\n")
                            .append(stringList.get(i));
                }
            }
            return stringBuilder.toString();
        }
        return this.yamlConfig.getString(key);
    }
}
