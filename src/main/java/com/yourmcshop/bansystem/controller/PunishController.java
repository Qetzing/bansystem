package com.yourmcshop.bansystem.controller;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.config.YamlConfig;
import com.yourmcshop.bansystem.data.PunishEntry;
import com.yourmcshop.bansystem.data.PunishUser;
import com.yourmcshop.bansystem.message.MessageType;
import com.yourmcshop.bansystem.model.DateTime;
import com.yourmcshop.bansystem.model.PunishFormat;
import com.yourmcshop.bansystem.model.PunishTime;
import com.yourmcshop.bansystem.model.PunishType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * Created by DevTastisch on 01.03.2019
 */
public class PunishController {

    private final BanSystem banSystem;
    private final YamlConfig banReasonConfig;
    private final Set<PunishFormat> punishFormats = ConcurrentHashMap.newKeySet();

    public PunishController(BanSystem banSystem) {
        this.banSystem = banSystem;
        this.banReasonConfig = YamlConfig.of(String.format("./plugins/%s/banreasons.yml", banSystem.getDescription().getName()));
        this.initBanReasons();
    }

    private void initBanReasons() {
        this.banReasonConfig.getKeys("banreasons").forEach(reasonId -> {
            int id = Integer.valueOf(reasonId);
            YamlConfig section = this.banReasonConfig.getSection(String.format("banreasons.%d", id));
            YamlConfig punish = section.getSection("punish");
            System.out.println("Create Banreason " + id);
            this.punishFormats.add(new PunishFormat(
                    id,
                    section.getString("displayname"),
                    section.getString("displaytype"),
                    section.getString("permission"),
                    punish.getKeys().stream().map(punishId -> {
                        System.out.println("Create Punish ID " + punishId + " for Ban " + reasonId);
                        YamlConfig punishSection = punish.getSection(punishId);
                        return new PunishTime(
                                Integer.valueOf(punishId),
                                punishSection.getLong("time"),
                                PunishType.valueOf(punishSection.getString("type"))
                        );
                    }).collect(Collectors.toList())

            ));
        });
    }


    public PunishFormat getFormatById(int id) {
        return this.punishFormats.stream()
                .filter(punishFormat -> punishFormat.getId() == id)
                .findAny()
                .orElse(null);
    }

    public PunishFormat getAutoIpBanFormat() {
        return this.getFormatById(this.banReasonConfig.getInt("autoipban"));
    }

    public boolean punish(CommandSender commandSender, ProxiedPlayer proxiedPlayer, int id, String ip) {
        return this.punish(commandSender, proxiedPlayer.getUniqueId(), id, ip);
    }

    public boolean punish(CommandSender commandSender, ProxiedPlayer proxiedPlayer, int reasonId, PunishTime punishTime, String ip) {
        return this.punish(commandSender, proxiedPlayer.getUniqueId(), reasonId, this.getNextTime(proxiedPlayer, reasonId), ip);
    }

    public boolean punish(CommandSender commandSender, UUID uuid, int id, String ip) {
        return this.punish(commandSender, uuid, id, getNextTime(uuid, id), ip);
    }

    public boolean punish(CommandSender commandSender, UUID uuid, PunishFormat punishFormat, String ip) {
        return this.punish(commandSender, uuid, punishFormat.getId(), ip);
    }

    public boolean punish(CommandSender commandSender, UUID uuid, String customReason, PunishType punishType, long duration, String ip) throws ExecutionException {
        if (!this.isPunished(uuid, punishType)) {
            try {
                PunishUser punishUser = BanSystem.getInstance().getPunishUserRepository().findByUniqueID(uuid).get();
                ProxyServer.getInstance().getPlayers().stream()
                        .filter(player -> player.hasPermission("bansystem.notify"))
                        .forEach(player -> {
                            if (punishType == PunishType.BAN) {
                                MessageType.INFO_BROADCAST_BAN.sendTo(player,
                                        punishUser,
                                        commandSender instanceof ProxiedPlayer ? commandSender.getName() : MessageType.FORMAT_CONSOLE_BAN
                                );
                            } else if (punishType == PunishType.MUTE) {
                                MessageType.INFO_BROADCAST_MUTE.sendTo(player,
                                        punishUser,
                                        commandSender instanceof ProxiedPlayer ? commandSender.getName() : MessageType.FORMAT_CONSOLE_BAN
                                );
                            }
                        });
                this.punish(new PunishEntry(
                        null,
                        punishType,
                        -1,
                        customReason,
                        true,
                        new DateTime(System.currentTimeMillis()),
                        duration == -1 ? null : new DateTime(System.currentTimeMillis() + duration),
                        punishUser,
                        commandSender instanceof ProxiedPlayer ? BanSystem.getInstance().getPunishUserRepository().findByUniqueID(((ProxiedPlayer) commandSender).getUniqueId()).get() : null,
                        ip
                ), uuid);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    public boolean unpunish(UUID uuid, PunishType punishType) {
        if (this.isPunished(uuid, punishType)) {
            try {
                PunishEntry punishEntry = BanSystem.getInstance().getPunishEntryRepository().findActiveByUUIDAndType(uuid, punishType).get();
                if (punishEntry == null) return false;
                punishEntry.setActive(false);
                if (punishEntry.isInfinity()) punishEntry.setEnding(null);
                banSystem.getPunishEntryRepository().save(punishEntry);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    private void punish(PunishEntry entry, UUID uuid) {
        ProxiedPlayer kickUser = ProxyServer.getInstance().getPlayer(uuid);
        BanSystem.getInstance().getPunishEntryRepository().save(entry);
        if (kickUser != null) {
            if (entry.getPunishType() == PunishType.BAN) {
                if (entry.isInfinity()) {
                    kickUser.disconnect(new TextComponent(MessageType.INFO_BAN_SCREEN_PERMANENT.format(
                            entry
                    )));
                } else {
                    kickUser.disconnect(MessageType.INFO_BAN_SCREEN.format(
                            entry
                    ));
                }
            }
        }


    }


    public boolean punish(CommandSender commandSender, UUID uuid, int reasonId, PunishTime punishTime, String ip) {
        try {
            if (!this.isPunished(uuid, punishTime.getPunishType())) {
                PunishUser punishUser = BanSystem.getInstance().getPunishUserRepository().findByUniqueID(uuid).get();
                PunishEntry entry = new PunishEntry(
                        null,
                        punishTime.getPunishType(),
                        reasonId,
                        null,
                        true,
                        new DateTime(System.currentTimeMillis()),
                        punishTime.getTime() == -1 ? null : new DateTime(System.currentTimeMillis() + punishTime.getTime() * 1000),
                        punishUser,
                        commandSender instanceof ProxiedPlayer ? BanSystem.getInstance().getPunishUserRepository().findByUniqueID(((ProxiedPlayer) commandSender).getUniqueId()).get() : null,
                        ip
                );
                ProxyServer.getInstance().getPlayers().stream()
                        .filter(player -> player.hasPermission("bansystem.notify"))
                        .forEach(player -> {
                            if (entry.getPunishType() == PunishType.BAN) {
                                MessageType.INFO_BROADCAST_BAN.sendTo(player,
                                        punishUser,
                                        commandSender instanceof ProxiedPlayer ? commandSender.getName() : MessageType.FORMAT_CONSOLE_BAN
                                );
                            } else if (entry.getPunishType() == PunishType.MUTE) {
                                MessageType.INFO_BROADCAST_MUTE.sendTo(player,
                                        punishUser,
                                        commandSender instanceof ProxiedPlayer ? commandSender.getName() : MessageType.FORMAT_CONSOLE_BAN
                                );
                            }
                        });

                this.punish(entry, uuid);
                return true;
            }

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return false;
    }

    private PunishTime getNextTime(ProxiedPlayer proxiedPlayer, int id) {
        return this.getNextTime(proxiedPlayer.getUniqueId(), id);
    }

    public PunishTime getNextTime(UUID uuid, int id) {
        try {
            PunishUser banUser = BanSystem.getInstance().getPunishUserRepository().findByUniqueID(uuid).get();
            final int count = (int) (banUser.getPunishEntries()
                    .stream()
                    .filter(banEntry -> banEntry.getReasonId() == id)
                    .count());

            PunishFormat formatById = this.getFormatById(id);
            PunishTime[] objects = formatById.getPunishTimes().stream()
                    .sorted(Comparator.comparingInt(PunishTime::getCount))
                    .toArray(PunishTime[]::new);

            if (count < objects.length) {
                return objects[count];
            }
            return objects[objects.length - 1];
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isPunished(ProxiedPlayer proxiedPlayer, PunishType punishType) {
        return this.isPunished(proxiedPlayer.getUniqueId(), punishType);
    }

    public boolean isPunished(UUID uuid, PunishType punishType) {
        try {
            return BanSystem.getInstance().getPunishUserRepository().findByUniqueID(uuid).get().isPunished(punishType);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<PunishFormat> getFormats() {
        return this.punishFormats.stream()
                .sorted(Comparator.comparingInt(PunishFormat::getId))
                .collect(Collectors.toList());
    }

}
