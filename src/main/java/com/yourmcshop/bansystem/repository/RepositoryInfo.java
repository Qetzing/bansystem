package com.yourmcshop.bansystem.repository;


import com.yourmcshop.bansystem.BanSystem;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention( RetentionPolicy.RUNTIME )
@Target( ElementType.TYPE )
public @interface RepositoryInfo {

    Class repositoryClass() default BanSystem.class;

}
