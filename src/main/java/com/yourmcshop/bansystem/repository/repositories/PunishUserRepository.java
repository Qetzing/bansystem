package com.yourmcshop.bansystem.repository.repositories;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishUser;
import com.yourmcshop.bansystem.repository.Repository;
import com.yourmcshop.bansystem.repository.RepositoryInfo;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by DevTastisch on 01.03.2019
 */
@RepositoryInfo(repositoryClass = BanSystem.class)
public class PunishUserRepository extends Repository<PunishUser> {

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    public Future<PunishUser> findById(int id) {
        return executorService.submit(() -> {
            try (Session session = this.getSessionFactory().getCurrentSession()) {
                session.beginTransaction();
                return session.get(PunishUser.class, id);
            }
        });
    }

    public Future<PunishUser> findByUniqueID(UUID uuid) {
        return executorService.submit(() -> {
            try (Session session = this.getSessionFactory().getCurrentSession()) {
                session.beginTransaction();
                return (PunishUser) session.createCriteria(PunishUser.class)
                        .add(Restrictions.eq("uuid", uuid))
                        .uniqueResult();
            }
        });
    }

    public Future<PunishUser> findByCachedNameIgnoreCase(String name) {
        return executorService.submit(() -> {
            try (Session session = this.getSessionFactory().getCurrentSession()) {
                session.beginTransaction();
                return (PunishUser) session.createCriteria(PunishUser.class)
                        .add(Restrictions.eq("cachedName", name).ignoreCase())
                        .uniqueResult();
            }
        });
    }

}
