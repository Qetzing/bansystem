package com.yourmcshop.bansystem.repository.repositories;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishEntry;
import com.yourmcshop.bansystem.model.PunishType;
import com.yourmcshop.bansystem.repository.Repository;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by DevTastisch on 01.03.2019
 */
public class PunishEntryRepository extends Repository<PunishEntry> {

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    public Future<PunishEntry> findById(String id, boolean ignoreInactive) {
        return executorService.submit(() -> {
            PunishEntry punishEntry;
            try (Session session = this.getSessionFactory().getCurrentSession()) {
                session.beginTransaction();
                punishEntry = (PunishEntry) session.createCriteria(PunishEntry.class)
                        .add(Restrictions.eq("punishId", id))
                        .uniqueResult();
            }
            if(ignoreInactive){
                return punishEntry;
            }else{
                return this.validateActive(punishEntry);
            }
        });
    }

    public Future<List<PunishEntry>> findByUUID(UUID uuid, boolean ignoreInactive) {
        return executorService.submit(() -> {
            List<PunishEntry> punishEntries;
            try (Session session = this.getSessionFactory().getCurrentSession()) {
                session.beginTransaction();
                punishEntries = (List<PunishEntry>) session.createCriteria(PunishEntry.class)
                        .add(Restrictions.eq("user.id", BanSystem.getInstance().getPunishUserRepository().findByUniqueID(uuid).get().getId()))
                        .list();
            }
            if(ignoreInactive) {
                return punishEntries;
            }else{
                return this.validateActiveList(punishEntries);
            }
            });
    }

    public Future<List<PunishEntry>> findActiveByIpAndType(String ip, PunishType punishType) {
        return executorService.submit(() -> {
            List<PunishEntry> punishEntries;
            try (Session session = this.getSessionFactory().getCurrentSession()) {
                session.beginTransaction();
                punishEntries = (List<PunishEntry>) session.createCriteria(PunishEntry.class)
                        .add(Restrictions.eq("ipAddress", ip))
                        .add(Restrictions.eq("active", true))
                        .add(Restrictions.eq("punishType", punishType))
                        .list();
            }
            return this.validateActiveList(punishEntries);
        });
    }

    public Future<PunishEntry> findActiveByUUIDAndType(UUID uuid, PunishType punishType) {
        return executorService.submit(() -> {
            PunishEntry punishEntry;
            try (Session session = this.getSessionFactory().getCurrentSession()) {
                session.beginTransaction();
                punishEntry = (PunishEntry) session.createCriteria(PunishEntry.class)
                        .add(Restrictions.eq("user.id", BanSystem.getInstance().getPunishUserRepository().findByUniqueID(uuid).get().getId()))
                        .add(Restrictions.eq("punishType", punishType))
                        .add(Restrictions.eq("active", true))
                        .uniqueResult();
            }
            return this.validateActive(punishEntry);
        });
    }

    private List<PunishEntry> validateActiveList(List<PunishEntry> punishEntries) {
        List<PunishEntry> validated = new LinkedList<>();
        for (PunishEntry punishEntry : punishEntries) {
            if (punishEntry.isActive() && (punishEntry.getEnding().getMillis() == -1 || punishEntry.getEnding().getMillis() > System.currentTimeMillis())) {
                validated.add(punishEntry);
            } else {
                punishEntry.setActive(false);
                this.save(punishEntry);
            }
        }
        return validated;
    }

    private PunishEntry validateActive(PunishEntry punishEntry) {
        if (punishEntry != null) {
            if (punishEntry.isActive()) {
                if (punishEntry.getEnding().getMillis() == -1 || punishEntry.getEnding().getMillis() > System.currentTimeMillis()) {
                    return punishEntry;
                } else {
                    punishEntry.setActive(false);
                    BanSystem.getInstance().getPunishEntryRepository().save(punishEntry);
                }
            }
        }
        return null;
    }


}
