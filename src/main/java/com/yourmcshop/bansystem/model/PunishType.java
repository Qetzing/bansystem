package com.yourmcshop.bansystem.model;

/**
 * Created by DevTastisch on 01.03.2019
 */
public enum PunishType {

    MUTE, BAN

}
