package com.yourmcshop.bansystem.model;

/**
 * Created by DevTastisch on 01.03.2019
 */
public class PunishTime {

    private final int count;
    private final long time;
    private final PunishType punishType;

    public PunishTime(int count, long time, PunishType punishType) {
        this.count = count;
        this.time = time;
        this.punishType = punishType;
    }


    public int getCount() {
        return count;
    }

    public long getTime() {
        return time;
    }

    public PunishType getPunishType() {
        return punishType;
    }
}
