package com.yourmcshop.bansystem.data;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.model.DateTime;
import com.yourmcshop.bansystem.model.PunishFormat;
import com.yourmcshop.bansystem.model.PunishType;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


/**
 * Created by DevTastisch on 01.03.2019
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "bansystem_punish_entries", indexes = @Index(name = "punishId", columnList = "punishId", unique = true))
public class PunishEntry {

    @Id
    @GenericGenerator(name = "punishIdGenerator", strategy = "com.yourmcshop.bansystem.data.PunishEntryIDGenerator")
    @GeneratedValue(generator = "punishIdGenerator", strategy = GenerationType.SEQUENCE)
    private String punishId;

    @Column(columnDefinition = "enum('MUTE','BAN')", nullable = false)
    @Enumerated(EnumType.STRING)
    private PunishType punishType;

    @Column(nullable = false)
    private int reasonId;

    private String customReason;

    @Column(columnDefinition = "enum('false','true')", nullable = false)
    @Convert(converter = BooleanEnumAttributeConverter.class)
    private boolean active;

    @Column(nullable = false)
    @Convert(converter = DateTimeTimestampAttributeConverter.class)
    private DateTime created;

    @Convert(converter = DateTimeTimestampAttributeConverter.class)
    private DateTime ending;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id")
    private PunishUser user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoker_id")
    private PunishUser invoker;

    private String ipAddress;

    /**
     * @return either the custom reason if set or otherwise {@link PunishFormat#getName()}.
     */
    public String getDisplayType() {
        return this.customReason == null ? this.getReason().getName() : this.customReason;
    }

    /**
     * @return the remaining time until the punish entry will become invalid in milliseconds.
     */
    public long getRemainingTime() {
        if (this.ending == null || this.ending.getMillis() == -1) return -1;
        return this.ending.getMillis() - System.currentTimeMillis();
    }

    /**
     * @return the complete time from creation to end of this punish in milliseconds.
     */
    public long getCompleteTime() {
        if (this.ending == null || this.ending.getMillis() == -1) return -1;
        return this.ending.getMillis() - this.created.getMillis();
    }

    /**
     * @return the remaining time until the punish entry will become invalid as {@link DateTime}.
     */
    public DateTime getRemaining() {
        if (this.isInfinity()) {
            return new DateTime(-1);
        }
        return new DateTime(this.ending.getMillis() - System.currentTimeMillis());
    }

    /**
     * @return the complete time from creation to end of this punish as {@link DateTime}.
     */
    public DateTime getComplete() {
        if (this.isInfinity()) {
            return new DateTime(-1);
        }
        return new DateTime(this.ending.getMillis() - this.created.getMillis());
    }

    /**
     * @return the {@link PunishFormat} depending on the set {@link PunishEntry#getReasonId()}.
     * Will return null if the punish is a custom punish created without {@link PunishFormat}.
     */
    public PunishFormat getReason() {
        return BanSystem.getInstance().getPunishController().getFormatById(this.reasonId);
    }

    /**
     * @return the current punish type. is always {@link PunishType#BAN} or {@link PunishType#MUTE}.
     */
    public PunishType getPunishType() {
        return punishType;
    }

    /**
     * @return the punished user.
     */
    public PunishUser getUser() {
        return user;
    }

    /**
     * @return the user which created this punish.
     */
    public PunishUser getInvoker() {
        return invoker;
    }

    /**
     * @return the reasonId of the template which this punish was created from.
     * returns -1 if this ban is a custom ban without @{@link PunishFormat}
     */
    public int getReasonId() {
        return reasonId;
    }

    /**
     * @return the custom reason or null if punish was created by a {@link PunishFormat}.
     */
    public String getCustomReason() {
        return customReason;
    }

    /**
     * @return if the current punish is active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @return the exact timestamp when the punish was created represented as @{@link DateTime}.
     */
    public DateTime getCreated() {
        return created;
    }

    /**
     * @return the exact timestamp when the punish ends represented as @{@link DateTime}.
     */
    public DateTime getEnding() {
        return ending;
    }

    /**
     * @return the unique identifier of this punish.
     * This will contain by default [a-zA-Z0-9]
     */
    public String getPunishId() {
        return punishId;
    }

    /**
     * @return the banned address, can either be the simple ipv4/6 format(91.4.249.35) or hostname (p5B04F925.dip0.t-ipconnect.de).
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * @return if the current punish is permanent.
     */
    public boolean isInfinity() {
        return this.getCompleteTime() == -1;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    public void setCreated(DateTime created) {
        this.created = created;
    }

    public void setPunishType(PunishType punishType) {
        this.punishType = punishType;
    }

    public void setReasonId(int reasonId) {
        this.reasonId = reasonId;
    }


    public void setCustomReason(String customReason) {
        this.customReason = customReason;
    }

    public void setEnding(DateTime ending) {
        this.ending = ending;
    }

    public void setUser(PunishUser user) {
        this.user = user;
    }

    public void setInvoker(PunishUser invoker) {
        this.invoker = invoker;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }


}
