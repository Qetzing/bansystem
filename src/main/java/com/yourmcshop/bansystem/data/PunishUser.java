package com.yourmcshop.bansystem.data;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.model.PunishFormat;
import com.yourmcshop.bansystem.model.PunishType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by DevTastisch on 01.03.2019
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "bansystem_user")
public class PunishUser implements Serializable {

    @Id
    @GenericGenerator(name = "increment", strategy = "increment")
    @GeneratedValue(generator = "increment")
    private Integer id;

    @Column(length = 36, nullable = false)
    @Type(type = "uuid-char")
    private UUID uuid;

    @Column(length = 16, nullable = false)
    private String cachedName;

    @Column(nullable = false)
    private String lastIp;

    @OneToMany(mappedBy = "user", orphanRemoval = true, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<PunishEntry> punishEntries;

    /**
     * @return either the active ban if {@link PunishUser#isBanned()} or null.
     */
    public PunishEntry getActiveBan() {
        return this.getActivePunish(PunishType.BAN);
    }

    /**
     * @return either the active ban if {@link PunishUser#isMuted()} ()} or null.
     */
    public PunishEntry getActiveMute() {
        return this.getActivePunish(PunishType.MUTE);
    }

    /**
     * @return user database id.
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the user Universally Unique Identifier(uuid) represented with Mojang standard.
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * @return the latest cached player name.
     */
    public String getCachedName() {
        return cachedName;
    }
    /**
     * @return the latest cached host address.
     * can either be the simple ipv4/6 format(91.4.249.35) or hostname (p5B04F925.dip0.t-ipconnect.de).
     */
    public String getLastIp() {
        return lastIp;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public void setCachedName(String cachedName) {
        this.cachedName = cachedName;
    }

    public PunishEntry getActivePunish(PunishType punishType) {
        return this.validateActive(this.punishEntries.stream()
                .filter(punishEntry -> punishEntry.isActive() && punishEntry.getPunishType().equals(punishType))
                .findAny()
                .orElse(null));
    }

    public List<PunishEntry> getPunishEntries(PunishFormat punishFormat) {
        return this.punishEntries.stream()
                .filter(punishEntry -> punishEntry.getReasonId() == punishFormat.getId())
                .collect(Collectors.toList());
    }

    public List<PunishEntry> getCustomPunishEntries() {
        return this.punishEntries.stream()
                .filter(punishEntry -> punishEntry.getReasonId() == -1)
                .collect(Collectors.toList());
    }

    public boolean isPunished(PunishType punishType) {
        return this.getActivePunish(punishType) != null;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp;
    }

    public List<PunishEntry> getPunishEntries() {
        return punishEntries;
    }

    public boolean isBanned() {
        PunishEntry activePunish = this.getActivePunish(PunishType.BAN);
        if (activePunish == null) return false;
        if (activePunish.getRemainingTime() != -1 && activePunish.getRemainingTime() < 0) {
            activePunish.setActive(false);
            BanSystem.getInstance().getPunishEntryRepository().save(activePunish);
            return false;
        } else {
            return true;
        }
    }

    public boolean isMuted() {
        PunishEntry activePunish = this.getActivePunish(PunishType.MUTE);
        if (activePunish == null) return false;
        if (activePunish.getRemainingTime() != -1 && activePunish.getRemainingTime() < 0) {
            activePunish.setActive(false);
            BanSystem.getInstance().getPunishEntryRepository().save(activePunish);
            return false;
        } else {
            return true;
        }
    }

    private PunishEntry validateActive(PunishEntry punishEntry) {
        if (punishEntry != null) {
            if (punishEntry.isActive()) {
                if (punishEntry.getEnding().getMillis() == -1 || punishEntry.getEnding().getMillis() > System.currentTimeMillis()) {
                    return punishEntry;
                } else {
                    punishEntry.setActive(false);
                    BanSystem.getInstance().getPunishEntryRepository().save(punishEntry);
                }
            }
        }
        return null;
    }

    public void setPunishEntries(List<PunishEntry> punishEntries) {
        this.punishEntries = punishEntries;
    }
}
