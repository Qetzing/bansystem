package com.yourmcshop.bansystem.data;

import com.yourmcshop.bansystem.BanSystem;
import nl.flotsam.xeger.Xeger;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;

/**
 * Created by DevTastisch on 03.03.2019
 */

public class PunishEntryIDGenerator implements IdentifierGenerator {

    private static final String generatorName = "punishIdGenerator";
    private Xeger xeger;

    public Serializable generate(SessionImplementor session, Object object) throws HibernateException {
        if(xeger == null) xeger = new Xeger(BanSystem.getInstance().getMainConfig().getString("settings.punishid"));
        return xeger.generate();
    }
}
